package com.example.kalkulator205150407111033
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity2 : AppCompatActivity() {
    lateinit var txtInput: TextView
    lateinit var textView3: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        txtInput = findViewById(R.id.textView3)
        var result = intent.getStringExtra("result")
        txtInput.text = result
    }
}